/*
Теоретичне питання
1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
   Конструкция try.. catch позволяет обрабатывать ошибки во время исполнения кода.
   Она позволяет запустить код и перехватить ошибки, которые могут в нём возникнуть.
   С помощью оператора throw можно создавать пользовательские ошибки.
*/

import { books } from "./array.js";

const listWrapper = document.querySelector('#root');
const bookList = document.createElement('ul');
listWrapper.append(bookList);

books.forEach( (item, number = 0) => {
    number++;
    const properties = ['author', 'name', 'price'];
    const missingProperties = [];

    try {
        properties.forEach( property => {
            if(!item.hasOwnProperty(property)){
                missingProperties.push(property);
            }
        });

        if(missingProperties.length === 0){
            const bookItems = document.createElement('li');
            const books = Object.entries(item).join(' // ');
            bookItems.textContent = books.toString().replace( /,/g, ": " );
            bookList.appendChild(bookItems);
        } else {
            throw new Error(`Ошибка! У книги №${number} отсутствует свойство: ${missingProperties.toString()}`);
        }

    } catch (err) {
        console.log(err.message);
    }

});